import { Injectable } from '@nestjs/common';
import { CreateTypeDto } from './dto/create-type.dto';
import { UpdateTypeDto } from './dto/update-type.dto';
import { Type } from './entities/type.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TypesService {
  constructor(
    @InjectRepository(Type) private typeRepository: Repository<Type>,
  ) {}
  create(createTypeDto: CreateTypeDto) {
    return this.typeRepository.save(createTypeDto);
  }

  findAll() {
    return this.typeRepository.find();
  }

  findOne(id: number) {
    return this.typeRepository.findOneBy({ id: id });
  }

  async update(id: number, updateTypeDto: UpdateTypeDto) {
    await this.typeRepository.findOneByOrFail({ id });
    await this.typeRepository.update(id, updateTypeDto);
    const updatedType = this.typeRepository.findOneBy({ id: id });
    return updatedType;
  }

  async remove(id: number) {
    const removeType = await this.typeRepository.findOneByOrFail({ id });
    await this.typeRepository.remove(removeType);
    return removeType;
  }
}
